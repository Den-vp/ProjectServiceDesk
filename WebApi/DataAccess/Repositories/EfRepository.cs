﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Abstractions.Repositories;

namespace WebApi.DataAccess.Repositories
{
    public class EfRepository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var entities = await _dataContext.Set<TEntity>().ToListAsync();

            return entities;
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }

        public async Task<IEnumerable<TEntity>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _dataContext.Set<TEntity>().Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task AddAsync(TEntity entity)
        {
            await _dataContext.Set<TEntity>().AddAsync(entity);

            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(TEntity entity)
        {
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _dataContext.Set<TEntity>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }
    }
}
