﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Domain.Entities;
using WebApp.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin"), Authorize(Roles = "admin")]
    public class ReferencesController : Controller
    {
        public IActionResult Index() => View();
        [HttpGet]
        public async Task<IActionResult> Departments()
        {
            string apiUrl = Config.EabrApi;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync("Departments");
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    List<Department> JsonObject = JsonConvert.DeserializeObject<List<Department>>(data);
                    ViewBag.Departments = JsonObject;
                }
            }
            return View();
        }
        
        [HttpGet]
        public IActionResult DepartmentsCreate() => View();
        [HttpPost]
        public async Task<IActionResult> DepartmentsCreate(Department model)
        {
            if (ModelState.IsValid)
            {
                string apiUrl = Config.EabrApi;
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        client.BaseAddress = new Uri(apiUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                        var response = await client.PostAsJsonAsync($"Departments", model);
                        response.EnsureSuccessStatusCode();
                        return RedirectToAction("Departments");
                    }
                    catch (Exception exception)
                    {
                        System.Diagnostics.Debug.WriteLine("EXCEPTION:");
                        System.Diagnostics.Debug.WriteLine(exception);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> DepartmentsUpdate(Guid id)
        {
            string apiUrl = Config.EabrApi;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync($"Departments/{id}");
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var JsonObject = JsonConvert.DeserializeObject<Department>(data);
                    Department model = new Department { Id = JsonObject.Id, Name = JsonObject.Name };
                    //ViewBag.Departments = JsonObject;
                    return View(model);
                }
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> DepartmentsUpdate(Department model)
        {
            if (ModelState.IsValid)
            {
                string apiUrl = Config.EabrApi;
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        client.BaseAddress = new Uri(apiUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                        var response = await client.PutAsJsonAsync($"Departments/{model.Id}", model);
                        response.EnsureSuccessStatusCode();
                        return RedirectToAction("Departments");
                    }
                    catch (Exception exception)
                    {
                        System.Diagnostics.Debug.WriteLine("EXCEPTION:");
                        System.Diagnostics.Debug.WriteLine(exception);
                    }
                }
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> DepartmentsDelete(string id)
        {
            if (ModelState.IsValid)
            {
                string apiUrl = Config.EabrApi;
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        client.BaseAddress = new Uri(apiUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                        var response = await client.DeleteAsync($"Departments/{id}");
                        response.EnsureSuccessStatusCode();
                        return RedirectToAction("Departments");
                    }
                    catch (Exception exception)
                    {
                        System.Diagnostics.Debug.WriteLine("EXCEPTION:");
                        System.Diagnostics.Debug.WriteLine(exception);
                    }
                }
            }
            return View();
        }
    }
}
