﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class addUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MiddleName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "SurName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0016aae4-11db-433f-b0a5-34c3e937206d",
                column: "ConcurrencyStamp",
                value: "52faa988-7a4c-4288-8477-82db9110947e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8aec76c5-9377-408c-bf58-92786282f5c0",
                columns: new[] { "ConcurrencyStamp", "Name", "PasswordHash", "SurName" },
                values: new object[] { "93595b40-35c1-44f9-81cf-9903180f5379", "Admin", "AQAAAAEAACcQAAAAEIjvwuFg6PceV9CAwF+Uo26mmkc5Ac+B0ecccXqYvTFAth8pnGhrV8wKB1bzvt2aiw==", "Admin" });

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("1dd4bd96-420e-45d8-be4d-d8aeadee026a"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 11, 36, 25, 380, DateTimeKind.Utc).AddTicks(5929));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("861c56bf-3c8e-4d88-a706-4b5b32e81952"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 11, 36, 25, 380, DateTimeKind.Utc).AddTicks(5896));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("bbf3ab8f-ecb3-4b36-8b20-ba4493132283"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 11, 36, 25, 380, DateTimeKind.Utc).AddTicks(3962));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MiddleName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SurName",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0016aae4-11db-433f-b0a5-34c3e937206d",
                column: "ConcurrencyStamp",
                value: "3371abf6-e659-44be-a634-a0246b80d4bc");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8aec76c5-9377-408c-bf58-92786282f5c0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6c3ebfe3-51d9-41a7-8f4a-1d4f39c6778a", "AQAAAAEAACcQAAAAEOoqLoIAxLBGndScYF1AcnV0JRiPiWJ2kgRNvH0pawe0+Dw0Agq8wWN523I+2dl++A==" });

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("1dd4bd96-420e-45d8-be4d-d8aeadee026a"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 13, 11, 49, 54, 927, DateTimeKind.Utc).AddTicks(3285));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("861c56bf-3c8e-4d88-a706-4b5b32e81952"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 13, 11, 49, 54, 927, DateTimeKind.Utc).AddTicks(3255));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("bbf3ab8f-ecb3-4b36-8b20-ba4493132283"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 13, 11, 49, 54, 927, DateTimeKind.Utc).AddTicks(1338));
        }
    }
}
